import { useAppSelector } from '../../../hooks/redux'
import { selectWeatherHourlyForecast } from '../../../redux/weather/hourly/slice'
import { Hour } from './Hour'
import { VerticalSliceWrapper } from '../../../components/VerticalSlice/components/VerticalSliceWrapper'

export const HourWrapper = () => {
  const forecast = useAppSelector(selectWeatherHourlyForecast)

  return (
    forecast !== undefined && (
      <VerticalSliceWrapper>
        {forecast.hour.map(hour => (
          <Hour key={hour.time} hour={hour} />
        ))}
      </VerticalSliceWrapper>
    )
  )
}
