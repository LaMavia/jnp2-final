import { styled } from 'styled-components'

export const TableCell = styled.span<{ align: AlignSetting }>`
  color: ${({ theme }) => theme.palette.primary.text};
  font: ${({ theme }) => theme.font};
  font-size: ${({ theme }) => theme.fontSize.small};
  text-align: ${({ align }) => align};
`
