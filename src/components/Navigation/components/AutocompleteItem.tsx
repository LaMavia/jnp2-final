import { styled } from 'styled-components'
import { WeatherLocation } from '../../../api/weather/types/common'
import { useAppDispatch } from '../../../hooks/redux'
import { setLocation, setLocationInput } from '../../../redux/location/slice'

export interface AutocompleteItemProps {
  item: WeatherLocation
  isSelected: boolean
}

const StyledLi = styled.li`
  background-color: ${({ theme }) => theme.palette.secondary.background};
  color: ${({ theme }) => theme.palette.secondary.text};
  cursor: pointer;
  width: 100%;
  font-size: ${({ theme }) => theme.fontSize.small};
  padding: 5px;

  &[js-selected='true'] {
    background-color: ${({ theme }) => theme.palette.primary.background};
    color: ${({ theme }) => theme.palette.primary.text};
  }
`

export const AutocompleteItem = ({
  item: { name, country, region },
  isSelected,
}: AutocompleteItemProps) => {
  const dispatch = useAppDispatch()

  const onClick = () => {
    dispatch(setLocationInput(name))
    dispatch(setLocation(name))
  }

  return (
    <StyledLi
      js-selected={`${isSelected}`}
      onClick={onClick}
      key={`${name}-${region}-${country}`}
    >{`${name}${region && ` (${region})`}, ${country}`}</StyledLi>
  )
}
