import { styled } from 'styled-components'
import { useAppSelector } from '../../hooks/redux'
import {
  selectWeatherHourlyLocation,
  selectWeatherHourlyNice,
} from '../../redux/weather/hourly/slice'
import { HourWrapper } from './components/HourWrapper'
import { LocationHeader } from '../../components/LocationHeader'

const OuterWrapper = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-between;
  height: 100%;
  max-width: 600px;
  min-width: 300px;
  flex-grow: 1;
`

export const HourlyView = () => {
  const location = useAppSelector(selectWeatherHourlyLocation)
  const niceness = useAppSelector(selectWeatherHourlyNice)

  return (
    location !== undefined && (
      <OuterWrapper>
        <LocationHeader location={location} niceness={niceness} />
        <HourWrapper />
      </OuterWrapper>
    )
  )
}
