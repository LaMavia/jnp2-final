import { z } from 'zod'
import { PexelsPhoto } from './common'

export type PexelsPhotoSearchResponse = z.infer<
  typeof PexelsPhotoSearchResponse
>
export const PexelsPhotoSearchResponse = z.object({
  total_results: z.number(),
  page: z.number(),
  per_page: z.number(),
  photos: z.array(PexelsPhoto).min(1),
  next_page: z.string().url(),
})
