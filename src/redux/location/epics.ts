import { Epic, StateObservable, combineEpics, ofType } from 'redux-observable'
import {
  addLocationToCache,
  searchLocation,
  setLocation,
  setLocationInput,
  setLocationToCurrent,
  setMatchingLocations,
} from './slice'
import { Observable, catchError, filter, from, map, mergeMap, of } from 'rxjs'
import { getWeatherSearch } from '../../api/weather'
import { AppAction, RootState } from '../app/store'
import { WeatherResponseException } from '../../api/weather/types'
import { pushError } from '../error/slice'
import { cacheContains, cacheGet } from '../../utils/cache'
import { GEOLOCATION_OPTIONS } from './consts'

const locationSearchEpic: Epic<AppAction> = (
  action$,
  state$: StateObservable<RootState>,
) => {
  const newSearch$ = (locationFragment: string) =>
    from(getWeatherSearch(locationFragment)).pipe(
      mergeMap(newMatching =>
        of(
          setMatchingLocations(newMatching),
          addLocationToCache({
            locationFragment,
            response: newMatching,
          }),
        ),
      ),
      catchError(({ error }: WeatherResponseException) => of(pushError(error))),
    )

  return action$.pipe(
    ofType(searchLocation.type),
    map(({ payload }) => payload),
    filter(location => location.length > 0),
    map(
      locationFragment =>
        [locationFragment, state$.value.location.searchCache] as const,
    ),
    mergeMap(([locationFragment, searchCache]) =>
      cacheContains(searchCache, locationFragment)
        ? of(setMatchingLocations(cacheGet(searchCache, locationFragment)!))
        : newSearch$(locationFragment),
    ),
  )
}

const locationCurrentEpic: Epic<AppAction> = action$ => {
  const getUserLocation$ = (): Observable<string> =>
    from(
      new Promise<string>((resolve, reject) => {
        navigator.geolocation.getCurrentPosition(
          ({ coords: { latitude, longitude } }) =>
            resolve(`${latitude},${longitude}`),
          e => {
            if (e !== undefined && e !== null) {
              reject(e.message)
            }
          },
          GEOLOCATION_OPTIONS,
        )
      }),
    )

  return action$.pipe(
    ofType(setLocationToCurrent.type),
    mergeMap(getUserLocation$),
    mergeMap(location => of(setLocation(location), setLocationInput(location))),
    catchError(e =>
      of(
        pushError({
          code: -2,
          message: `${e}`,
        }),
      ),
    ),
  )
}

export const locationEpic = combineEpics(
  locationSearchEpic,
  locationCurrentEpic,
)
