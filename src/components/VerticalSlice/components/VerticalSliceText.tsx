import { styled } from 'styled-components'
import { fontSmall } from '../../../redux/theme/utils/fontSize'

export const VerticalSliceText = styled.span`
  color: ${({ theme }) => theme.palette.primary.text};
  font-size: ${fontSmall};
  font-family: ${({ theme }) => theme.font};
  text-align: center;
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-evenly;
  align-items: center;

  & svg {
    fill: ${({ theme }) => theme.palette.primary.text};
    height: ${fontSmall};
    width: ${fontSmall};
  }
`
