import { styled } from 'styled-components'
import { Tab } from './components/Tab'
import { TabContainer } from './components/TabContainer'
import { SearchBar } from './components/SearchBar'
import { WeatherType } from '../../redux/weather/type/slice'

const StyledNav = styled.nav`
  background-color: ${({ theme }) => theme.palette.primary.background};
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  align-items: center;
`

export const Navigation = () => (
  <StyledNav>
    <SearchBar />
    <TabContainer>
      <Tab weatherType={WeatherType.Current} label='current' />
      <Tab weatherType={WeatherType.Hourly} label='hourly' />
      <Tab weatherType={WeatherType.Daily} label='daily' />
    </TabContainer>
  </StyledNav>
)
