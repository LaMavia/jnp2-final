import { combineReducers } from '@reduxjs/toolkit'
import {
  WeatherCurrentAction,
  weatherCurrentReducer,
  weatherCurrentStoreName,
} from './current/slice'
import {
  WeatherTypeAction,
  weatherTypeReducer,
  weatherTypeStoreName,
} from './type/slice'
import {
  WeatherDailyAction,
  weatherDailyReducer,
  weatherDailyStoreName,
} from './daily/slice'
import {
  WeatherHourlyAction,
  weatherHourlyReducer,
  weatherHourlyStoreName,
} from './hourly/slice'

export type WeatherAction =
  | WeatherCurrentAction
  | WeatherTypeAction
  | WeatherDailyAction
  | WeatherHourlyAction

export const weatherStoreName = 'weather'
export const weatherReducer = combineReducers({
  [weatherCurrentStoreName]: weatherCurrentReducer,
  [weatherTypeStoreName]: weatherTypeReducer,
  [weatherDailyStoreName]: weatherDailyReducer,
  [weatherHourlyStoreName]: weatherHourlyReducer,
})
