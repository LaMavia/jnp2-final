import { styled } from 'styled-components'

export const TableRow = styled.li`
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
  padding: 10px;
  border-bottom: 1px solid ${({ theme }) => theme.palette.accent.background};

  &:last-of-type {
    border-bottom: none;
  }
`
