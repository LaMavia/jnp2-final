import { WeatherForecastDay } from '../../../api/weather/types/common'
import {
  NICE_MAX_AVG_TEMP,
  NICE_MAX_TEMP,
  NICE_MIN_AVG_TEMP,
  NICE_MIN_TEMP,
} from '../consts'
import { WeatherNice } from '../types'

export const isNice = (numberOfNiceAttributes: number): WeatherNice =>
  numberOfNiceAttributes <= 1
    ? WeatherNice.Enum['not nice']
    : numberOfNiceAttributes <= 2
    ? WeatherNice.Enum.passable
    : WeatherNice.Enum.nice

export const dayHasRainyDays = (forecast: WeatherForecastDay) =>
  forecast.day.daily_will_it_rain

export const dayHasBoundedAvgTemp = (forecast: WeatherForecastDay) =>
  NICE_MIN_AVG_TEMP <= forecast.day.avgtemp_c &&
  forecast.day.avgtemp_c <= NICE_MAX_AVG_TEMP

export const dayHasBoundedTemp = (forecast: WeatherForecastDay) =>
  NICE_MIN_TEMP <= forecast.day.mintemp_c &&
  forecast.day.maxtemp_c <= NICE_MAX_TEMP
