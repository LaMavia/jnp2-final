import { styled } from 'styled-components'
import { useAppDispatch, useAppSelector } from '../../hooks/redux'
import { selectThemeKind, switchTheme } from '../../redux/theme/slice'
import { ThemeKind } from '../../redux/theme/types'
import { ReactElement } from 'react'
import { DarkIcon } from './assets/DarkIcon'
import { LightIcon } from './assets/LightIcon'

const StyledButton = styled.button`
  position: fixed;
  z-index: 3;
  top: 10px;
  right: 10px;
  height: 2rem;
  width: auto;
  color: ${({ theme }) => theme.palette.primary.text};
  border: none;
  background: transparent;
  cursor: pointer;

  & * {
    height: 100%;
    fill: ${({ theme }) => theme.palette.primary.text};
  }
`

export const ThemeSwitch = () => {
  const themeKind = useAppSelector(selectThemeKind)
  const dispatch = useAppDispatch()

  const iconMap: Record<ThemeKind, ReactElement> = {
    [ThemeKind.Dark]: <DarkIcon />,
    [ThemeKind.Light]: <LightIcon />,
  }

  return (
    <StyledButton onClick={() => dispatch(switchTheme())}>
      {iconMap[themeKind]}
    </StyledButton>
  )
}
