import { PayloadAction, createSlice } from '@reduxjs/toolkit'
import {
  WeatherForecastDay,
  WeatherLocation,
} from '../../../api/weather/types/common'
import { SliceActions, SliceState } from '../../../utils/redux'
import { cachePut, makeCache } from '../../../utils/cache'
import { RootState } from '../../app/store'
import {
  dayHasBoundedAvgTemp,
  dayHasBoundedTemp,
  dayHasRainyDays,
  isNice,
} from '../utils/nice'

interface WeatherDailyCache {
  forecasts: WeatherForecastDay[]
  location: WeatherLocation
}

const initialState = {
  forecasts: [] as WeatherForecastDay[],
  location: undefined as WeatherLocation | undefined,
  cache: makeCache<string, WeatherDailyCache>(),
}

const weatherDailySlice = createSlice({
  name: 'daily',
  initialState,
  reducers: {
    setWeatherDaily: (
      state,
      { payload: { forecasts, location } }: PayloadAction<WeatherDailyCache>,
    ) => {
      state.forecasts = forecasts
      state.location = location
    },
    fetchWeatherDaily: (_state, _action: PayloadAction<string>) => {
      void _state
      void _action
    },
    dailyAddToCache: (
      state,
      {
        payload: { location, response },
      }: PayloadAction<{ location: string; response: WeatherDailyCache }>,
    ) => {
      cachePut(state.cache, location, response)
    },
  },
})

export type WeatherDailyAction = SliceActions<typeof weatherDailySlice>
export type WeatherDailyState = SliceState<typeof weatherDailySlice>

export const weatherDailyStoreName = weatherDailySlice.name
export const weatherDailyReducer = weatherDailySlice.reducer
export const { fetchWeatherDaily, setWeatherDaily, dailyAddToCache } =
  weatherDailySlice.actions

export const selectWeatherDailyForecasts = ({ weather }: RootState) =>
  weather.daily.forecasts
export const selectWeatherDailyLocation = ({ weather }: RootState) =>
  weather.daily.location
export const selectWeatherDailyHasRainyDays = ({ weather }: RootState) =>
  weather.daily.forecasts.some(dayHasRainyDays)
export const selectWeatherDailyHasBoundedAvgTemp = ({ weather }: RootState) =>
  weather.daily.forecasts.every(dayHasBoundedAvgTemp)
export const selectWeatherDailyHasBoundedTemp = ({ weather }: RootState) =>
  weather.daily.forecasts.every(dayHasBoundedTemp)
export const selectWeatherDailyNice = (state: RootState) =>
  isNice(
    [
      selectWeatherDailyHasRainyDays,
      selectWeatherDailyHasBoundedAvgTemp,
      selectWeatherDailyHasBoundedTemp,
    ]
      .map(predicate => predicate(state))
      .map(Number)
      .reduce((sum, v) => sum + v, 0),
  )
