import { LocationHeader } from '../../../components/LocationHeader'
import { useAppSelector } from '../../../hooks/redux'
import {
  selectWeatherDailyLocation,
  selectWeatherDailyNice,
} from '../../../redux/weather/daily/slice'

export const Header = () => {
  const location = useAppSelector(selectWeatherDailyLocation)
  const niceness = useAppSelector(selectWeatherDailyNice)

  return (
    location !== undefined && (
      <LocationHeader location={location} niceness={niceness} />
    )
  )
}
