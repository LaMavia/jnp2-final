import { WeatherForecastHour } from '../../../../../api/weather/types/common'
import { VerticalSliceText } from '../../../../../components/VerticalSlice/components/VerticalSliceText'
import { VerticalSliceTextWrapper } from '../../../../../components/VerticalSlice/components/VerticalSliceTextContainer'
import { getUserLocale } from '../../../../../utils/locale'

export const HourTime = ({ hour }: { hour: WeatherForecastHour }) => {
  const userLocale = getUserLocale()
  const rawTime = new Date(Date.parse(hour.time))
  const formattedTime = new Intl.DateTimeFormat(userLocale, {
    timeStyle: 'short',
  }).format(rawTime)

  return (
    <VerticalSliceTextWrapper gridArea='time'>
      <VerticalSliceText>{formattedTime}</VerticalSliceText>
    </VerticalSliceTextWrapper>
  )
}
