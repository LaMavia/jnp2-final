import { styled } from 'styled-components'
import { fontSmall } from '../../redux/theme/utils/fontSize'

export const VerticalSlice = styled.div`
  --width: calc(0px + 4 * ${fontSmall});
  --paddedTextHeight: calc(10px + ${fontSmall});

  height: 100%;
  flex-grow: 1;
  border: none;
  border-right: 2px solid ${({ theme }) => theme.palette.accent.background};
  display: grid;
  grid-gap: 10px;
  padding: 10px;

  &:last-of-type {
    border-right: none;
  }
`
