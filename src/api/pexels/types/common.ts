import { z } from 'zod'

export type PexelsPhotoSource = z.infer<typeof PexelsPhotoSource>
export const PexelsPhotoSource = z.object({
  original: z.string().url(),
  large2x: z.string().url(),
  large: z.string().url(),
  medium: z.string().url(),
  small: z.string().url(),
  portrait: z.string().url(),
  landscape: z.string().url(),
  tiny: z.string().url(),
})

export type PexelsPhoto = z.infer<typeof PexelsPhoto>
export const PexelsPhoto = z.object({
  id: z.number(),
  width: z.number(),
  height: z.number(),
  url: z.string().url(),
  photographer: z.string(),
  photographer_url: z.string().url(),
  photographer_id: z.number(),
  avg_color: z.string().regex(/^#[0-9A-F]{6}$/i),
  src: PexelsPhotoSource,
  liked: z.boolean(),
  alt: z.string(),
})
