import { Theme, ThemeKind } from './types'

export const themes: Record<ThemeKind, Theme> = {
  [ThemeKind.Dark]: {
    borderRadius: '5px',
    palette: {
      primary: {
        background: '#221d23',
        text: '#ffffff',
      },
      secondary: {
        background: '#788aa3',
        text: '#ffffff',
      },
      accent: {
        background: '#f4c095',
        text: '#1b1b1b',
      },
    },
    font: 'sans-serif',
    fontSize: {
      large: '3em',
      medium: '2em',
      small: '1em',
    },
  },
  [ThemeKind.Light]: {
    borderRadius: '5px',
    palette: {
      primary: {
        background: '#ffffff',
        text: '#221d23',
      },
      secondary: {
        background: '#cad5e3',
        text: '#1b1b1b',
      },
      accent: {
        background: '#f4c095',
        text: '#1b1b1b',
      },
    },
    font: 'sans-serif',
    fontSize: {
      large: '3em',
      medium: '2em',
      small: '1em',
    },
  },
}
