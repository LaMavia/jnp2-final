import { styled } from 'styled-components'
import { WeatherForecastHour } from '../../../../api/weather/types/common'
import { VerticalSliceIcon } from '../../../../components/VerticalSlice/components/VerticalSliceIcon'
import { VerticalSliceTextWrapper } from '../../../../components/VerticalSlice/components/VerticalSliceTextContainer'
import { VerticalSliceText } from '../../../../components/VerticalSlice/components/VerticalSliceText'
import { VerticalSlice } from '../../../../components/VerticalSlice'
import { HourTime } from './components/HourTime'
import { CloudIcon } from '../../../../assets/icons/CloudIcon'
import { PrecipitationIcon } from '../../../../assets/icons/PrecipitationIcon'
import { HumidityIcon } from '../../../../assets/icons/HumidityIcon'

const OuterWrapper = styled(VerticalSlice)`
  grid-template:
    'icon         ' var(--width)
    'temp         ' calc(2 * var(--paddedTextHeight))
    'wind         ' calc(2 * var(--paddedTextHeight))
    'pressure     ' var(--paddedTextHeight)
    'precipitation' var(--paddedTextHeight)
    'cloud        ' var(--paddedTextHeight)
    'humidity     ' var(--paddedTextHeight)
    '_            ' 1fr
    'time         ' calc(2 * var(--paddedTextHeight))
    / 1fr;
`

export const Hour = ({ hour }: { hour: WeatherForecastHour }) => (
  <OuterWrapper>
    <VerticalSliceIcon src={hour.condition.icon} />
    <VerticalSliceTextWrapper gridArea='temp'>
      <VerticalSliceText>
        {hour.temp_c}°C ({hour.feelslike_c}°C)
      </VerticalSliceText>
    </VerticalSliceTextWrapper>
    <VerticalSliceTextWrapper gridArea='wind'>
      <VerticalSliceText>
        {hour.wind_dir} {hour.wind_kph}km/h
      </VerticalSliceText>
    </VerticalSliceTextWrapper>
    <VerticalSliceTextWrapper gridArea='pressure'>
      <VerticalSliceText>{hour.pressure_mb}mbar</VerticalSliceText>
    </VerticalSliceTextWrapper>
    <VerticalSliceTextWrapper gridArea='precipitation'>
      <VerticalSliceText>
        <PrecipitationIcon />
        {hour.precip_mm}mm
      </VerticalSliceText>
    </VerticalSliceTextWrapper>
    <VerticalSliceTextWrapper gridArea='cloud'>
      <VerticalSliceText>
        <CloudIcon />
        {hour.cloud}%
      </VerticalSliceText>
    </VerticalSliceTextWrapper>
    <VerticalSliceTextWrapper gridArea='humidity'>
      <VerticalSliceText>
        <HumidityIcon />
        {hour.humidity}%
      </VerticalSliceText>
    </VerticalSliceTextWrapper>
    <HourTime hour={hour} />
  </OuterWrapper>
)
