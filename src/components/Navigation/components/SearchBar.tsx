import { Autocomplete } from 'react-rac'
import { styled } from 'styled-components'
import {
  searchLocation,
  selectLocationInput,
  selectMatchingLocations,
  setLocation,
  setLocationInput,
  setLocationToCurrent,
} from '../../../redux/location/slice'
import { useAppDispatch, useAppSelector } from '../../../hooks/redux'
import { WeatherLocation } from '../../../api/weather/types/common'
import { AutocompleteItem } from './AutocompleteItem'
import { SearchIcon } from '../../../assets/icons/SearchIcon'
import { fontSmall } from '../../../redux/theme/utils/fontSize'
import { MyLocationIcon } from '../../../assets/icons/MyLocationIcon'

const StyledAutocomplete = styled(
  Autocomplete as typeof Autocomplete<WeatherLocation>,
)``

const StyledContainer = styled.div`
  position: relative;
  z-index: 2;
  max-width: 700px;
  width: 100%;
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;

  & input[role='combobox'] {
    background-color: ${({ theme }) => theme.palette.primary.background};
    color: ${({ theme }) => theme.palette.primary.text};
    font-size: ${({ theme }) => theme.fontSize.small};
    border: none;
    border-bottom: 2px solid ${({ theme }) => theme.palette.accent.background};
    padding: 10px;
    margin: 10px;

    & ~ div {
      & > div {
        border: none;
        background-color: ${({ theme }) => theme.palette.secondary.background};
      }
    }
  }
`

const StyledSearchButton = styled.button`
  --size: calc(25.5px + ${fontSmall});
  --border: solid 2px ${({ theme }) => theme.palette.accent.background};

  height: var(--size);
  width: var(--size);
  padding: 5px;
  background-color: ${({ theme }) => theme.palette.primary.background};
  fill: ${({ theme }) => theme.palette.primary.text};
  font-size: ${({ theme }) => theme.fontSize.small};
  border: var(--border);
  border-left: none;
  cursor: pointer;
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;

  &:hover,
  &:focus {
    background-color: ${({ theme }) => theme.palette.accent.background};
    fill: ${({ theme }) => theme.palette.accent.text};

    transition: fill 0.2s 0s ease-in-out, background-color 0.2s 0s ease-in-out;
  }

  & svg {
    height: ${fontSmall};
    width: 100%;
  }
`

const StyledLocationButton = styled(StyledSearchButton)`
  border-left: var(--border);
  border-right: none;
`

export const SearchBar = () => {
  const locationInput = useAppSelector(selectLocationInput)
  const matchingLocations = useAppSelector(selectMatchingLocations)
  const dispatch = useAppDispatch()

  const onLocationInputChange = (e: InputEvent) => {
    const locationFragment = (e.target as HTMLInputElement)?.value

    dispatch(setLocationInput(locationFragment))
    dispatch(searchLocation(locationFragment))
  }

  const onSearchClick = () => {
    dispatch(setLocation(locationInput))
  }

  const onMyLocationClick = () => {
    dispatch(setLocationToCurrent())
  }

  return (
    <StyledContainer>
      <StyledLocationButton onClick={onMyLocationClick}>
        <MyLocationIcon />
      </StyledLocationButton>
      <StyledAutocomplete
        value={locationInput}
        onChange={onLocationInputChange}
        items={matchingLocations}
        getItemValue={({ name }) => name}
        shouldItemRender={({ name }, value) => name.startsWith(value)}
        renderItem={(item, isSelected) => (
          <div
            key={`${item.name}-${item.region}-${item.country}-${item.lat}-${item.lon}`}
          >
            <AutocompleteItem item={item} isSelected={isSelected} />
          </div>
        )}
      />
      <StyledSearchButton onClick={onSearchClick}>
        <SearchIcon />
      </StyledSearchButton>
    </StyledContainer>
  )
}
