import { Navigation } from '../../components/Navigation'
import { PropsWithChildren } from 'react'
import { ThemeSwitch } from '../../components/ThemeSwitch'
import { Loader } from '../../components/Loader'
import { styled } from 'styled-components'

const StyledOuterContainer = styled.div`
  display: flex;
  flex-flow: column nowrap;
  width: 100%;
  height: 100%;
`

const StyledContentContainer = styled.div`
  display: flex;
  flex-flow: row nowrap;
  flex-grow: 1;
  overflow: hidden auto;
  padding: 10px;
  align-items: center;
  justify-content: center;
  overflow: hidden;
`

export const SearchLayout = ({ children }: PropsWithChildren) => (
  <StyledOuterContainer>
    <Navigation />
    <ThemeSwitch />
    <Loader />
    <StyledContentContainer>{children}</StyledContentContainer>
  </StyledOuterContainer>
)
