import { Epic, StateObservable, combineEpics, ofType } from 'redux-observable'
import { AppAction, RootState } from '../../app/store'
import { withLoader } from '../../../utils/redux'
import { catchError, from, map, mergeMap, of } from 'rxjs'
import { dailyAddToCache, fetchWeatherDaily, setWeatherDaily } from './slice'
import { cacheContains, cacheGet } from '../../../utils/cache'
import { getWeatherDaily } from '../../../api/weather'
import { FORECAST_LENGTH } from './consts'
import { WeatherResponseException } from '../../../api/weather/types'
import { pushError } from '../../error/slice'

const locationFetchEpic: Epic<AppAction> = (
  action$,
  state$: StateObservable<RootState>,
) => {
  const newSearch$ = (location: string) =>
    withLoader(
      from(getWeatherDaily(location, FORECAST_LENGTH)).pipe(
        map(({ forecast: { forecastday }, location }) => ({
          forecasts: forecastday,
          location,
        })),
        mergeMap(response =>
          of(
            setWeatherDaily(response),
            dailyAddToCache({
              location,
              response,
            }),
          ),
        ),
        catchError(exn => {
          console.error(exn)
          const { error }: WeatherResponseException = exn
          return of(pushError(error))
        }),
      ),
    )

  return action$
    .pipe(
      ofType(fetchWeatherDaily.type),
      map(({ payload }) => payload),
      map(newWeatherType => [newWeatherType, state$.value] as const),
    )
    .pipe(
      mergeMap(
        ([
          location,
          {
            weather: {
              daily: { cache },
            },
          },
        ]) =>
          cacheContains(cache, location)
            ? of(setWeatherDaily(cacheGet(cache, location)!))
            : newSearch$(location),
      ),
    )
}

export const weatherDailyEpic = combineEpics(locationFetchEpic)
