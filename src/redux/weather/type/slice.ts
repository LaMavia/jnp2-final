import { PayloadAction, createSlice } from '@reduxjs/toolkit'
import { SliceActions } from '../../../utils/redux'
import { RootState } from '../../app/store'

export enum WeatherType {
  Current = 'Weather/Current',
  Daily = 'Weather/Daily',
  Hourly = 'Weather/Hourly',
}

const initialState = {
  details: WeatherType.Current,
}

const weatherTypeSlice = createSlice({
  name: 'type',
  initialState,
  reducers: {
    setWeatherType: (
      state,
      { payload: newType }: PayloadAction<WeatherType>,
    ) => {
      state.details = newType
    },
  },
})

export type WeatherTypeAction = SliceActions<typeof weatherTypeSlice>

export const weatherTypeStoreName = weatherTypeSlice.name
export const weatherTypeReducer = weatherTypeSlice.reducer
export const { setWeatherType } = weatherTypeSlice.actions

export const selectWeatherType = ({ weather: { type } }: RootState) =>
  type.details
