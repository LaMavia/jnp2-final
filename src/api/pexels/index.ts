import { fetchApi, makeApiUrl } from './_internal/fetchApi'
import { PexelsPhotoSearchResponse } from './types'

export const getPexelsSearch = (searchPhrase: string) =>
  fetchApi(
    makeApiUrl('search', {
      query: searchPhrase,
      orientation: 'landscape',
      per_page: 1,
    }),
    {
      method: 'GET',
    },
  ).then(PexelsPhotoSearchResponse.parse)
