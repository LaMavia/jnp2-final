import { combineEpics } from 'redux-observable'
import { locationEpic } from '../location/epics'
import { weatherEpic } from '../weather/epics'
import { backgroundEpic } from '../background/epics'

export const rootEpic = combineEpics(locationEpic, weatherEpic, backgroundEpic)
export type RootEpic = typeof rootEpic
