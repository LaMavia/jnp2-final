import { DefaultTheme } from 'styled-components'

type Selector<T = unknown> = <Args extends { theme: DefaultTheme }>({
  theme,
}: Args) => T

export const fontLarge: Selector<string> = ({ theme }) => theme.fontSize.large
export const fontMedium: Selector<string> = ({ theme }) => theme.fontSize.medium
export const fontSmall: Selector<string> = ({ theme }) => theme.fontSize.small
