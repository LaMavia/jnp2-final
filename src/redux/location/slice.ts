import { Action, PayloadAction, createSlice } from '@reduxjs/toolkit'
import { RootState } from '../app/store'
import { WeatherResponseSearch } from '../../api/weather/types'
import { cachePut, makeCache } from '../../utils/cache'
import { SliceActions, SliceState } from '../../utils/redux'

// Since redux toolkit uses immer internally,
// it's safe to declare the initial state
// as a native object
// https://redux-toolkit.js.org/usage/immer-reducers#redux-toolkit-and-immer
const initialState = {
  location: '',
  locationInput: '',
  matchingLocations: [] as WeatherResponseSearch,
  searchCache: makeCache<string, WeatherResponseSearch>(),
}

const locationSlice = createSlice({
  name: 'location',
  initialState,
  reducers: {
    setLocationInput: (
      state,
      { payload: newLocationInput }: PayloadAction<string>,
    ) => {
      state.locationInput = newLocationInput
    },
    setLocation: (state, { payload: newLocation }: PayloadAction<string>) => {
      state.location = newLocation
    },
    setLocationToCurrent: (_state, _action: Action) => {
      void _state
      void _action
    },
    setMatchingLocations: (
      state,
      { payload: newMatchingLocations }: PayloadAction<WeatherResponseSearch>,
    ) => {
      state.matchingLocations = newMatchingLocations
    },
    searchLocation: (_state, _action: PayloadAction<string>) => {
      void _state
      void _action
    },
    addLocationToCache: (
      state,
      {
        payload: { locationFragment, response },
      }: PayloadAction<{
        locationFragment: string
        response: WeatherResponseSearch
      }>,
    ) => {
      cachePut(state.searchCache, locationFragment, response)
    },
  },
})

export type LocationAction = SliceActions<typeof locationSlice>
export type LocationState = SliceState<typeof locationSlice>

export const locationReducer = locationSlice.reducer
export const locationStoreName = locationSlice.name
export const {
  setLocation,
  setLocationInput,
  addLocationToCache,
  searchLocation,
  setMatchingLocations,
  setLocationToCurrent,
} = locationSlice.actions

export const selectLocation = ({ location }: RootState) => location.location
export const selectLocationInput = ({ location }: RootState) =>
  location.locationInput
export const selectMatchingLocations = ({ location }: RootState) =>
  location.matchingLocations
