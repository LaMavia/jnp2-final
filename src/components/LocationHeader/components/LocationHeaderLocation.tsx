import { styled } from 'styled-components'
import { fontMedium } from '../../../redux/theme/utils/fontSize'
import { WeatherLocation } from '../../../api/weather/types/common'

const StyledText = styled.span`
  font-size: ${fontMedium};
  font-family: ${({ theme }) => theme.font};
  color: ${({ theme }) => theme.palette.primary.text};
  text-transform: capitalize;
`

export const LocationHeaderLocation = ({
  location,
}: {
  location: WeatherLocation
}) => (
  <StyledText>
    {location?.name}, {location?.country}
  </StyledText>
)
