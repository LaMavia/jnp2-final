import { styled } from 'styled-components'

export const HeaderConditionIcon = styled.img`
  grid-area: icon;
  justify-self: center;
`
