export const API_KEY = import.meta.env.VITE_PEXELS_API_KEY
export const API_BASE = 'https://api.pexels.com/v1/'
