import { makeUrlQuery } from '../../../utils/url'
import { API_BASE, API_KEY } from '../consts'
import { PexelsException } from '../types/exception'

export const makeApiUrl = (
  path: string,
  query: Record<string, string | number | boolean>,
) => `${API_BASE}${path}?${makeUrlQuery(query)}`

export const fetchApi = <T extends object>(
  url: string,
  init?: RequestInit,
): Promise<T> =>
  fetch(url, {
    ...init,
    headers: {
      ...init?.headers,
      Accept: 'application/json',
      'Content-Type': 'application/json',
      'User-Agent': 'Pexels/JavaScript',
      Authorization: API_KEY,
    },
  })
    .then(r => {
      if (!r.ok) {
        const exception: PexelsException = {
          code: r.status,
          description: r.statusText,
          url: r.url,
        }

        throw PexelsException.parse(exception)
      }

      return r.json()
    })
    .catch((e: Error) => {
      const pexelsExceptionParse = PexelsException.safeParse(e)

      // the error has already been caught
      if (pexelsExceptionParse.success) {
        throw e
      }

      // handle a native error
      const exception: PexelsException = {
        code: -1,
        description: e.message,
        url,
      }

      throw PexelsException.parse(exception)
    })
