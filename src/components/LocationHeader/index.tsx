import { styled } from 'styled-components'
import { WeatherLocation } from '../../api/weather/types/common'
import { LocationHeaderLocation } from './components/LocationHeaderLocation'
import { WeatherNice } from '../../redux/weather/types'
import { LocationHeaderNiceness } from './components/LocationHeaderNiceness'

const StyledHeader = styled.header`
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  align-items: center;
  padding: 20px 0;
  flex-grow: 1;
  text-align: center;
`

export const LocationHeader = ({
  location,
  niceness,
}: {
  location: WeatherLocation
  niceness: WeatherNice
}) => (
  <StyledHeader>
    <LocationHeaderLocation location={location} />
    <LocationHeaderNiceness niceness={niceness} />
  </StyledHeader>
)
