import { styled } from 'styled-components'
import { TableRow } from './components/TableRow'
import { TableCell } from './components/TableCell'

export interface TableProps {
  entries: [string | number, string | number][]
}

const StyledContainer = styled.ul`
  display: flex;
  flex-flow: column nowrap;
  /* background-color: ${({ theme }) => theme.palette.primary.background}; */
  list-style: none;
  border: 1px solid ${({ theme }) => theme.palette.accent.background};
  padding: 0;
  margin: 0;
  backdrop-filter: blur(50px);
`

export const Table = ({ entries }: TableProps) => (
  <StyledContainer>
    {entries.map(([k, v], i) => (
      <TableRow key={`${k}:${i}`}>
        <TableCell align='left'>{k}</TableCell>
        <TableCell align='right'>{v}</TableCell>
      </TableRow>
    ))}
  </StyledContainer>
)
