import { z } from 'zod'

export type WeatherNice = z.infer<typeof WeatherNice>
export const WeatherNice = z.enum(['nice', 'passable', 'not nice'])
