import { styled } from 'styled-components'
import {
  WeatherType,
  selectWeatherType,
  setWeatherType,
} from '../../../redux/weather/type/slice'
import { useDispatch } from 'react-redux'
import { useAppSelector } from '../../../hooks/redux'

export interface TabProps {
  label: string
  weatherType: WeatherType
}

const StyledNavLink = styled.button<{ active: boolean }>`
  position: relative;
  display: box;
  font-size: ${({ theme }) => theme.fontSize.small};
  color: ${({ theme, active }) =>
    active ? theme.palette.accent.background : theme.palette.primary.text};
  padding: 5px 10px;
  text-decoration: none;
  text-transform: lowercase;
  font: menu;
  background-color: transparent;
  border: none;
  cursor: pointer;

  &::after {
    --width: ${({ active }) => (active ? '100%' : '50%')};

    content: '';
    position: absolute;
    bottom: 0;
    left: calc((100% - var(--width)) / 2);
    margin: auto;
    height: 2px;
    width: var(--width);
    border-radius: ${({ theme }) => theme.borderRadius};
    background-color: ${({ theme }) => theme.palette.accent.background};
    transition: transform 0.1s 0s ease-in-out;
  }

  &:hover {
    &::after {
      transform: scaleX(150%);
    }
  }
`

export const Tab = ({ label, weatherType }: TabProps) => {
  const dispatch = useDispatch()
  const currentWeatherType = useAppSelector(selectWeatherType)

  return (
    <StyledNavLink
      active={currentWeatherType === weatherType}
      onClick={() => dispatch(setWeatherType(weatherType))}
    >
      {label}
    </StyledNavLink>
  )
}
