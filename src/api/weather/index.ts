import {
  WeatherResponseCurrent,
  WeatherResponseDaily,
  WeatherResponseHourly,
  WeatherResponseSearch,
} from './types'
import { fetchApi, makeApiUrl } from './_internal/fetchApi'

export const getWeatherCurrent = (
  location: string,
): Promise<WeatherResponseCurrent> =>
  fetchApi(makeApiUrl('current.json', { q: location, aqi: 'no' }), {
    method: 'GET',
  }).then(WeatherResponseCurrent.parse)

export const getWeatherDaily = (
  location: string,
  days: number,
): Promise<WeatherResponseDaily> =>
  fetchApi(
    makeApiUrl('forecast.json', { q: location, days, aqi: 'no', alerts: 'no' }),
    {
      method: 'GET',
    },
  ).then(WeatherResponseDaily.parse)

export const getWeatherHourly = (
  location: string,
): Promise<WeatherResponseHourly> =>
  fetchApi(
    makeApiUrl('forecast.json', {
      q: location,
      days: 1,
      aqi: 'no',
      alerts: 'no',
    }),
    {
      method: 'GET',
    },
  ).then(WeatherResponseHourly.parse)

export const getWeatherSearch = (
  locationFragment: string,
): Promise<WeatherResponseSearch> =>
  fetchApi(makeApiUrl('search.json', { q: locationFragment }), {
    method: 'GET',
  }).then(WeatherResponseSearch.parse)
