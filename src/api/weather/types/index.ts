import { z } from 'zod'
import {
  WeatherCurrent,
  WeatherError,
  WeatherForecastDay,
  WeatherLocation,
} from './common'

export type WeatherResponseException = z.infer<typeof WeatherResponseException>
export const WeatherResponseException = z.object({
  error: WeatherError,
})

export type WeatherResponseCurrent = z.infer<typeof WeatherResponseCurrent>
export const WeatherResponseCurrent = z.object({
  location: WeatherLocation,
  current: WeatherCurrent,
})

export type WeatherResponseSearch = z.infer<typeof WeatherResponseSearch>
export const WeatherResponseSearch = z.array(WeatherLocation)

export type WeatherResponseDaily = z.infer<typeof WeatherResponseDaily>
export const WeatherResponseDaily = z.object({
  location: WeatherLocation,
  current: WeatherCurrent,
  forecast: z.object({
    forecastday: z.array(WeatherForecastDay),
  }),
})

export type WeatherResponseHourly = z.infer<typeof WeatherResponseHourly>
export const WeatherResponseHourly = WeatherResponseDaily.transform(
  ({ forecast, ...response }) => ({
    ...response,
    day: forecast.forecastday[0]!,
  }),
)
