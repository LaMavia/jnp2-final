import { Epic, StateObservable, combineEpics, ofType } from 'redux-observable'
import { weatherCurrentEpic } from './current/epics'
import { AppAction, RootState } from '../app/store'
import { Observable, mergeMap, of } from 'rxjs'
import { setLocation } from '../location/slice'
import { fetchWeatherCurrent } from './current/slice'
import { WeatherType } from './type/slice'
import { fetchWeatherDaily } from './daily/slice'
import { weatherDailyEpic } from './daily/epics'
import { weatherHourlyEpic } from './hourly/epics'
import { fetchWeatherHourly } from './hourly/slice'
import { weatherTypeEpic } from './type/epics'

const currentController = (location: string) => {
  void location
  return of(fetchWeatherCurrent({ forceFetch: true }))
}
const dailyController = (location: string) => of(fetchWeatherDaily(location))
const hourlyController = (location: string) => of(fetchWeatherHourly(location))

const controllerEpicSwitch = (
  { payload: location }: ReturnType<typeof setLocation>,
  state: RootState,
): Observable<AppAction> => {
  const switchMap: Record<
    WeatherType,
    (location: string) => Observable<AppAction>
  > = {
    [WeatherType.Current]: currentController,
    [WeatherType.Daily]: dailyController,
    [WeatherType.Hourly]: hourlyController,
  }

  return switchMap[state.weather.type.details](location)
}

const controllerEpic: Epic<AppAction> = (
  action$,
  state$: StateObservable<RootState>,
) =>
  action$.pipe(
    ofType(setLocation.type),
    mergeMap(action => controllerEpicSwitch(action, state$.value)),
  )

export const weatherEpic = combineEpics(
  controllerEpic,
  weatherCurrentEpic,
  weatherDailyEpic,
  weatherHourlyEpic,
  weatherTypeEpic,
)
