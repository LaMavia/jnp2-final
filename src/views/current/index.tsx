import { styled } from 'styled-components'
import { useAppSelector } from '../../hooks/redux'
import { selectWeatherCurrentForecast } from '../../redux/weather/current/slice'
import { Table } from '../../components/Table'
import { ForecastHeader } from './components/Header'
import { selectBackground } from '../../redux/background/slice'

const ImageContainer = styled.div<{ image?: string }>`
  background-image: url(${({ image }) => image});
  background-size: cover;
  background-position: center;
  background-repeat: no-repeat;
  backdrop-filter: blur(5px);
  width: 100vw;
  height: 100%;
  display: inherit;
  flex-flow: inherit;
  justify-content: inherit;
`
const StyledContainer = styled.div`
  --max-width: 600px;

  position: relative;
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-between;
  height: 100%;
  max-width: var(--max-width);
  flex-grow: 1;

  &::before {
    content: '';
    width: 200vw;
    height: 100vh;
    position: absolute;
    left: -100vw;
    top: 0;
    z-index: -1;
    background-color: ${({ theme }) => theme.palette.primary.background}CA;
  }
`

export const CurrentView = () => {
  const forecast = useAppSelector(selectWeatherCurrentForecast)
  const backgroundImage = useAppSelector(selectBackground)

  return (
    forecast !== undefined && (
      <ImageContainer image={backgroundImage?.src.large2x}>
        <StyledContainer>
          <ForecastHeader />
          <Table entries={Object.entries(forecast)} />
        </StyledContainer>
      </ImageContainer>
    )
  )
}
