import { PayloadAction, createSlice } from '@reduxjs/toolkit'
import {
  WeatherForecastDay,
  WeatherLocation,
} from '../../../api/weather/types/common'
import { cachePut, makeCache } from '../../../utils/cache'
import { SliceActions, SliceState } from '../../../utils/redux'
import { RootState } from '../../app/store'
import {
  dayHasBoundedAvgTemp,
  dayHasBoundedTemp,
  dayHasRainyDays,
  isNice,
} from '../utils/nice'
import { WeatherNice } from '../types'

interface WeatherHourlyCache {
  forecast: WeatherForecastDay
  location: WeatherLocation
}

const initialState = {
  forecast: undefined as WeatherForecastDay | undefined,
  location: undefined as WeatherLocation | undefined,
  cache: makeCache<string, WeatherHourlyCache>(),
}

const slice = createSlice({
  name: 'hourly',
  initialState,
  reducers: {
    setWeatherHourly: (
      state,
      { payload: { forecast, location } }: PayloadAction<WeatherHourlyCache>,
    ) => {
      state.forecast = forecast
      state.location = location
    },
    fetchWeatherHourly: (_state, _action: PayloadAction<string>) => {
      void _state
      void _action
    },
    hourlyAddToCache: (
      state,
      {
        payload: { location, response },
      }: PayloadAction<{ location: string; response: WeatherHourlyCache }>,
    ) => {
      cachePut(state.cache, location, response)
    },
  },
})

export type WeatherHourlyAction = SliceActions<typeof slice>
export type WeatherHourlyState = SliceState<typeof slice>

export const weatherHourlyStoreName = slice.name
export const weatherHourlyReducer = slice.reducer
export const { fetchWeatherHourly, hourlyAddToCache, setWeatherHourly } =
  slice.actions

export const selectWeatherHourlyForecast = ({ weather }: RootState) =>
  weather.hourly.forecast
export const selectWeatherHourlyLocation = ({ weather }: RootState) =>
  weather.hourly.location
export const selectWeatherHourlyNice = (state: RootState) => {
  const forecast = selectWeatherHourlyForecast(state)

  return forecast === undefined
    ? WeatherNice.enum['not nice']
    : isNice(
        [dayHasRainyDays, dayHasBoundedAvgTemp, dayHasBoundedTemp]
          .map(predicate => predicate(forecast))
          .map(Number)
          .reduce((sum, v) => sum + v, 0),
      )
}
