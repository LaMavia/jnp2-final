import { styled } from 'styled-components'
import { WeatherForecastDay } from '../../../../api/weather/types/common'
import { DayTemperature } from './components/DayTemperature'
import { DayDate } from './components/DayDate'
import { VerticalSlice } from '../../../../components/VerticalSlice'
import { VerticalSliceIcon } from '../../../../components/VerticalSlice/components/VerticalSliceIcon'
import { VerticalSliceTextWrapper } from '../../../../components/VerticalSlice/components/VerticalSliceTextContainer'
import { VerticalSliceText } from '../../../../components/VerticalSlice/components/VerticalSliceText'
import { PrecipitationIcon } from '../../../../assets/icons/PrecipitationIcon'
import { HumidityIcon } from '../../../../assets/icons/HumidityIcon'

export interface DayProps {
  forecast: WeatherForecastDay
}

const OuterContainer = styled(VerticalSlice)`
  grid-template:
    'icon         ' var(--width)
    'temp         ' var(--paddedTextHeight)
    'wind         ' var(--paddedTextHeight)
    'precipitation' var(--paddedTextHeight)
    'humidity     ' var(--paddedTextHeight)
    '_            ' 1fr
    'date         ' calc(2 * var(--paddedTextHeight))
    / 1fr;
`

export const Day = ({ forecast }: DayProps) => (
  <OuterContainer>
    <VerticalSliceIcon src={forecast.day.condition.icon} />
    <DayTemperature forecast={forecast} />
    <DayDate forecast={forecast} />
    <VerticalSliceTextWrapper gridArea='wind'>
      <VerticalSliceText>{forecast.day.maxwind_kph}km/h</VerticalSliceText>
    </VerticalSliceTextWrapper>
    <VerticalSliceTextWrapper gridArea='precipitation'>
      <VerticalSliceText>
        <PrecipitationIcon />
        {forecast.day.totalprecip_mm}mm
      </VerticalSliceText>
    </VerticalSliceTextWrapper>
    <VerticalSliceTextWrapper gridArea='humidity'>
      <VerticalSliceText>
        <HumidityIcon />
        {forecast.day.avghumidity}%
      </VerticalSliceText>
    </VerticalSliceTextWrapper>
  </OuterContainer>
)
