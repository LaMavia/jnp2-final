import { PayloadAction, createSlice } from '@reduxjs/toolkit'
import { WeatherCurrent } from '../../../api/weather/types/common'
import { SliceActions, SliceState } from '../../../utils/redux'
import { RootState } from '../../app/store'
import { WeatherResponseCurrent } from '../../../api/weather/types'
import { cachePut, makeCache } from '../../../utils/cache'
import { WeatherCurrentForecast } from './types'
import { z } from 'zod'

const initialState = {
  details: undefined as WeatherCurrent | undefined,
  cache: makeCache<string, WeatherResponseCurrent>(),
}

const weatherCurrentSlice = createSlice({
  name: 'current',
  initialState,
  reducers: {
    setWeatherCurrent: (
      state,
      { payload: newCurrentWeather }: PayloadAction<WeatherCurrent>,
    ) => {
      state.details = newCurrentWeather
    },
    fetchWeatherCurrent: (
      _state,
      _action: PayloadAction<{ forceFetch?: boolean }>,
    ) => {
      void _state
      void _action
    },
    addToCache: (
      state,
      {
        payload: { location, response },
      }: PayloadAction<{
        location: string
        response: WeatherResponseCurrent
      }>,
    ) => {
      cachePut(state.cache, location, response)
    },
  },
})

export type WeatherCurrentAction = SliceActions<typeof weatherCurrentSlice>
export type WeatherCurrentState = SliceState<typeof weatherCurrentSlice>

export const weatherCurrentStoreName = weatherCurrentSlice.name
export const weatherCurrentReducer = weatherCurrentSlice.reducer
export const { setWeatherCurrent, addToCache, fetchWeatherCurrent } =
  weatherCurrentSlice.actions

export const selectWeatherCurrentIcon = ({ weather: { current } }: RootState) =>
  current.details?.condition.icon
export const selectWeatherCurrentForecast = ({
  weather: {
    current: { details },
  },
}: RootState) =>
  details === undefined
    ? undefined
    : WeatherCurrentForecast.parse({
        temp: details?.temp_c,
        tempUnit: 'C',
        tempFeelsLike: details?.feelslike_c,
        cloudCoverage: details?.cloud,
        dateTime: new Date(Date.now()).toISOString(),
        description: details?.condition.text,
        humidity: details?.humidity,
        precipitation: details?.precip_mm,
        precipitationUnit: 'mm',
        pressure: details?.pressure_mb,
        pressureUnit: 'mbar',
        uv: details?.uv,
        windDirection: details?.wind_dir,
        windSpeed: details?.wind_kph,
        windSpeedUnit: 'km/h',
      } as z.input<typeof WeatherCurrentForecast>)
