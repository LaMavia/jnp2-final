export const makeUrlQuery = (
  query: Record<string, string | number | boolean>,
) =>
  Object.entries(query)
    .map(([k, v]) => `${k}=${encodeURIComponent(v)}`)
    .join('&')
