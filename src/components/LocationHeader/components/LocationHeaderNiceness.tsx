import { styled } from 'styled-components'
import { fontSmall } from '../../../redux/theme/utils/fontSize'
import { WeatherNice } from '../../../redux/weather/types'

const StyledText = styled.span`
  font-size: ${fontSmall};
  font-family: ${({ theme }) => theme.font};
  color: ${({ theme }) => theme.palette.primary.text};
  text-transform: capitalize;
`

export const LocationHeaderNiceness = ({
  niceness,
}: {
  niceness: WeatherNice
}) => <StyledText>the weather will be {niceness}</StyledText>
