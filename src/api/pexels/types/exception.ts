import { z } from 'zod'

export type PexelsException = z.infer<typeof PexelsException>
export const PexelsException = z.object({
  code: z
    .number({ description: 'http response code' })
    .min(400)
    .max(499)
    .or(z.literal(-1)),
  description: z.string(),
  url: z.string(),
})
