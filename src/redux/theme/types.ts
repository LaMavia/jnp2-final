export enum ThemeKind {
  Dark = 'ThemeKind:Dark',
  Light = 'ThemeKind:Light',
}

export interface Palette {
  background: string
  text: string
}

export interface Theme {
  borderRadius: string
  font: string
  palette: {
    primary: Palette
    secondary: Palette
    accent: Palette
  }
  fontSize: {
    large: string
    medium: string
    small: string
  }
}
