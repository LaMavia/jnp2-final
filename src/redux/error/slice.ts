import { Action, PayloadAction, createSlice } from '@reduxjs/toolkit'
import { WeatherError } from '../../api/weather/types/common'
import { SliceActions, SliceState } from '../../utils/redux'
import { RootState } from '../app/store'

const initialState: WeatherError[] = []

const errorSlice = createSlice({
  name: 'error',
  initialState,
  reducers: {
    pushError: (state, { payload: newError }: PayloadAction<WeatherError>) => {
      state.push(newError)
    },
    popError: (state, _action: Action) => {
      void _action
      state.pop()
    },
    removeError: (state, { payload: i }: PayloadAction<number>) => {
      state.splice(i, 1)
    },
  },
})

export type ErrorAction = SliceActions<typeof errorSlice>
export type ErrorState = SliceState<typeof errorSlice>

export const errorReducer = errorSlice.reducer
export const errorStoreName = errorSlice.name
export const { pushError, popError, removeError } = errorSlice.actions

export const selectHasErrors = ({ error }: RootState) => error.length > 0
export const selectErrors = ({ error }: RootState) => error
