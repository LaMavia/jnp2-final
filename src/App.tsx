import { ThemeProvider, createGlobalStyle } from 'styled-components'
import { useAppSelector } from './hooks/redux'
import { selectTheme } from './redux/theme/slice'
import { Router } from './Router'

const GlobalStyles = createGlobalStyle`
  body {
    display: flex;
    width: 100vw;
    min-height: 100vh;
    background-color: ${({ theme }) => theme.palette.primary.background};
    margin: 0;
    padding: 0;
  }

  * {
    box-sizing: border-box;
  }

  #root {
    display: flex;
    width: 100%;
    height: 100vh;
    padding: 0;
    margin: 0;
  }
`

export const App = () => {
  const theme = useAppSelector(selectTheme)

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      <Router />
    </ThemeProvider>
  )
}
