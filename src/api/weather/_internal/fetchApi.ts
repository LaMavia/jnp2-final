import { ZodError } from 'zod'
import { makeUrlQuery } from '../../../utils/url'
import { API_BASE, API_KEY } from '../consts'
import { WeatherResponseException } from '../types'

export const makeApiUrl = (
  path: string,
  query: Record<string, string | number | boolean>,
) => `${API_BASE}${path}?${makeUrlQuery({ ...query, key: API_KEY })}`

export const fetchApi = <T extends object>(
  url: string,
  init?: RequestInit,
): Promise<T> =>
  fetch(url, init)
    .then(r => r.json() as T | WeatherResponseException)
    .catch(
      (e): WeatherResponseException => ({
        error: {
          code: -1,
          message: e.message,
        },
      }),
    )
    .then(r => {
      // Ensure that `WeatherResponseException`
      // only gets thrown in `catch`
      if ('error' in r) {
        return WeatherResponseException.parseAsync(r)
          .catch(
            (e: ZodError): WeatherResponseException => ({
              error: {
                code: -1,
                message: e.errors.join(','),
              },
            }),
          )
          .then(e => {
            throw e
          })
      }

      return r
    })
