import { styled } from 'styled-components'
import { DayWrapper } from './components/DayWrapper'
import { LocationHeader } from '../../components/LocationHeader'
import { useAppSelector } from '../../hooks/redux'
import {
  selectWeatherDailyLocation,
  selectWeatherDailyNice,
} from '../../redux/weather/daily/slice'

const OuterWrapper = styled.div`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-between;
  height: 100%;
  max-width: 600px;
  flex-grow: 1;
`

export const DailyView = () => {
  const location = useAppSelector(selectWeatherDailyLocation)
  const niceness = useAppSelector(selectWeatherDailyNice)

  return (
    location !== undefined && (
      <OuterWrapper>
        <LocationHeader location={location} niceness={niceness} />
        <DayWrapper />
      </OuterWrapper>
    )
  )
}
