import { PayloadAction, createSlice } from '@reduxjs/toolkit'
import { PexelsPhoto } from '../../api/pexels/types/common'
import { cachePut, makeCache } from '../../utils/cache'
import { SliceActions, SliceState } from '../../utils/redux'
import { RootState } from '../app/store'

const initialState = {
  photo: undefined as PexelsPhoto | undefined,
  cache: makeCache<string, PexelsPhoto>(),
}

const backgroundSlice = createSlice({
  name: 'background',
  initialState,
  reducers: {
    setBackground: (
      state,
      { payload: newBackground }: PayloadAction<PexelsPhoto>,
    ) => {
      state.photo = newBackground
    },
    fetchBackground: (_state, _action: PayloadAction<string>) => {
      void _state, _action
    },
    addBackgroundToCache: (
      state,
      {
        payload: { searchPhrase, photo },
      }: PayloadAction<{
        searchPhrase: string
        photo: PexelsPhoto
      }>,
    ) => {
      cachePut(state.cache, searchPhrase, photo)
    },
  },
})

export type BackgroundAction = SliceActions<typeof backgroundSlice>
export type BackgroundState = SliceState<typeof backgroundSlice>

export const backgroundReducer = backgroundSlice.reducer
export const backgroundStoreName = backgroundSlice.name
export const { addBackgroundToCache, fetchBackground, setBackground } =
  backgroundSlice.actions

export const selectHasBackground = ({ background }: RootState) =>
  background.photo !== undefined

export const selectBackground = ({ background }: RootState) => background.photo
