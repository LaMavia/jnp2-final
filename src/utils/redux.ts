import { PayloadActionCreator, Slice } from '@reduxjs/toolkit'
import { Observable, concat, of } from 'rxjs'
import { setLoading } from '../redux/loading/slice'
import { AppAction } from '../redux/app/store'

export type SliceActions<
  S extends Slice,
  Keys extends keyof S['actions'] = keyof S['actions'],
> = ReturnType<S['actions'][Keys]>

export type ActionPayload<Creator extends PayloadActionCreator> =
  ReturnType<Creator>['payload']

export type SliceState<S extends Slice> = ReturnType<S['getInitialState']>

export const withLoader = <T extends AppAction>(
  observable$: Observable<T>,
): Observable<AppAction> =>
  concat(of(setLoading(true)), observable$, of(setLoading(false)))
