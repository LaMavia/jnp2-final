import { PropsWithChildren } from 'react'
import { styled } from 'styled-components'

const StyledContainer = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: space-evenly;
  max-width: 700px;
  width: 100%;
`

export const TabContainer = ({ children }: PropsWithChildren) => (
  <StyledContainer>{children}</StyledContainer>
)
