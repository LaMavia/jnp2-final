import { createSlice } from '@reduxjs/toolkit'
import { RootState } from '../app/store'
import { ThemeKind } from './types'
import { themes } from './consts'
import { SliceActions } from '../../utils/redux'

const initialState = {
  kind: ThemeKind.Dark,
}

const themeSlice = createSlice({
  name: 'theme',
  initialState,
  reducers: {
    switchTheme: state => {
      switch (state.kind) {
        case ThemeKind.Dark:
          state.kind = ThemeKind.Light
          break
        case ThemeKind.Light:
          state.kind = ThemeKind.Dark
          break
      }
    },
  },
})

export type ThemeAction = SliceActions<typeof themeSlice>

export const themeReducer = themeSlice.reducer
export const themeStoreName = themeSlice.name
export const { switchTheme } = themeSlice.actions

export const selectThemeKind = ({ theme }: RootState) => theme.kind
export const selectTheme = ({ theme }: RootState) => themes[theme.kind]
