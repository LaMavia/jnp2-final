import { equals } from 'ramda'

export interface Cache<K, V> {
  map: [K, V][]
}

export const makeCache = <K, V>(): Cache<K, V> => ({ map: [] })

export const cacheContains = <K, V>(cache: Cache<K, V>, key: K) =>
  cache.map.some(([k]) => equals(k, key))

export const cacheGet = <K, V>(cache: Cache<K, V>, key: K): V | undefined =>
  cache.map.reduce(
    (u, [k, v]) => u ?? (equals(k, key) ? v : undefined),
    undefined as V | undefined,
  )

export const cachePut = <K, V>(cache: Cache<K, V>, key: K, value: V) => {
  const valueInCache = cacheGet(cache, key)

  if (equals(value, valueInCache)) {
    return
  }

  if (valueInCache === undefined) {
    cache.map.push([key, value])
  } else {
    for (const entry of cache.map) {
      if (equals(entry[0] as K, key)) {
        entry[1] = value
        break
      }
    }
  }
}
