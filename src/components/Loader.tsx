import { DotLoader } from 'react-spinners'
import { useAppSelector } from '../hooks/redux'
import { selectIsLoading } from '../redux/loading/slice'
import { styled } from 'styled-components'

const StyledContainer = styled.dialog`
  background-color: rgba(0, 0, 0, 0.5);
  position: fixed;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;
  overflow: hidden;
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  align-items: center;
  z-index: 50;
`

export const Loader = () => {
  const isLoading = useAppSelector(selectIsLoading)

  return (
    isLoading && (
      <StyledContainer
        open
        onClose={e => (e.preventDefault(), e.stopPropagation())}
      >
        <DotLoader color='#ffffff' />
      </StyledContainer>
    )
  )
}
