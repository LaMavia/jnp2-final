import { PayloadAction, createSlice } from '@reduxjs/toolkit'
import { SliceActions } from '../../utils/redux'
import { RootState } from '../app/store'

const loadingSlice = createSlice({
  name: 'loading',
  initialState: { numberOfActiveLoaders: 0 },
  reducers: {
    setLoading: (state, { payload: newIsLoading }: PayloadAction<boolean>) => {
      state.numberOfActiveLoaders += newIsLoading ? 1 : -1
    },
  },
})

export type LoadingAction = SliceActions<typeof loadingSlice>

export const loadingStoreName = loadingSlice.name
export const loadingReducer = loadingSlice.reducer
export const { setLoading } = loadingSlice.actions

export const selectIsLoading = ({ loading }: RootState) =>
  loading.numberOfActiveLoaders > 0
