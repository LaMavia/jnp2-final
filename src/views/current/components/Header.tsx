import { styled } from 'styled-components'
import { fontLarge, fontSmall } from '../../../redux/theme/utils/fontSize'
import { HeaderDate } from './HeaderDate'
import { HeaderTemp } from './HeaderTemp'
import { useAppSelector } from '../../../hooks/redux'
import { selectWeatherCurrentIcon } from '../../../redux/weather/current/slice'
import { HeaderConditionIcon } from './HeaderConditionIcon'

const StyledHeader = styled.header`
  display: grid;
  grid-template:
    '_1        _1  ' 1fr
    'temp      icon' calc(20px + ${fontSmall} + ${fontLarge})
    'date-time icon' calc(10px + ${fontSmall})
    '_2        _2  ' 1fr
    / 2fr 1fr;
  align-items: start;
  justify-content: left;
  row-gap: 10px;
  padding: 20px 0;
  flex-grow: 1;
`

export const ForecastHeader = () => {
  const iconSource = useAppSelector(selectWeatherCurrentIcon)

  return (
    <StyledHeader>
      <HeaderDate />
      <HeaderTemp />
      <HeaderConditionIcon src={iconSource} />
    </StyledHeader>
  )
}
