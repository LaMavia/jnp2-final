import { z } from 'zod'

export type WeatherWindDirection = z.infer<typeof WeatherWindDirection>
const windDirectionFragments = 'NEWS'.split('')
export const WeatherWindDirection = z
  .string()
  .min(1)
  .max(3)
  .refine(dir =>
    dir.split('').every(letter => windDirectionFragments.includes(letter)),
  )

export type WeatherShortTime = z.infer<typeof WeatherShortTime>
const WeatherShortTime = z
  .string()
  .regex(/^(?:[0-1][0-9]|2[0-4]):(?:[0-6][0-9]) [AP]M$/)

export type WeatherShortDateTime = z.infer<typeof WeatherShortDateTime>
export const WeatherShortDateTime = z
  .string()
  .regex(
    /^[1-9]\d{3}-(?:0[0-9]|1[1-2])-(?:[0-2][0-9]|3[0-1]) (?:[0-1][0-9]|2[0-4]):(?:[0-6][0-9])$/,
  )

export const WeatherShortDate = z
  .string()
  .regex(/^[1-9]\d{3}-(?:0[0-9]|1[1-2])-(?:[0-2][0-9]|3[0-1])$/)

export type WeatherCondition = z.infer<typeof WeatherCondition>
export const WeatherCondition = z.object({
  text: z.string({ description: 'text description of the condition' }),
  icon: z.string({ description: 'url of the icon' }),
  code: z.number(),
})

export type WeatherCurrent = z.infer<typeof WeatherCurrent>
export const WeatherCurrent = z.object({
  last_updated_epoch: z.number(),
  last_updated: z.string(),
  temp_c: z.number(),
  temp_f: z.number(),
  is_day: z.number().transform(Boolean),
  condition: WeatherCondition,
  wind_mph: z.number(),
  wind_kph: z.number(),
  wind_degree: z.number(),
  wind_dir: WeatherWindDirection,
  pressure_mb: z.number(),
  pressure_in: z.number(),
  precip_mm: z.number(),
  precip_in: z.number(),
  humidity: z.number(),
  cloud: z.number(),
  feelslike_c: z.number(),
  feelslike_f: z.number(),
  vis_km: z.number(),
  vis_miles: z.number(),
  uv: z.number(),
  gust_mph: z.number(),
  gust_kph: z.number(),
})

export type WeatherLocation = z.infer<typeof WeatherLocation>
export const WeatherLocation = z.object({
  name: z.string(),
  region: z.string(),
  country: z.string(),
  lat: z.number(),
  lon: z.number(),
})

export type WeatherForecastDayDetails = z.infer<
  typeof WeatherForecastDayDetails
>
export const WeatherForecastDayDetails = z.object({
  maxtemp_c: z.number(),
  maxtemp_f: z.number(),
  mintemp_c: z.number(),
  mintemp_f: z.number(),
  avgtemp_c: z.number(),
  avgtemp_f: z.number(),
  maxwind_mph: z.number(),
  maxwind_kph: z.number(),
  totalprecip_mm: z.number(),
  totalprecip_in: z.number(),
  totalsnow_cm: z.number(),
  avgvis_km: z.number(),
  avgvis_miles: z.number(),
  avghumidity: z.number(),
  daily_will_it_rain: z.number().transform(Boolean),
  daily_chance_of_rain: z.number().min(0).max(100),
  daily_will_it_snow: z.number().min(0).max(100),
  daily_chance_of_snow: z.number(),
  condition: WeatherCondition,
  uv: z.number().min(0).max(11),
})

export type WeatherAstro = z.infer<typeof WeatherAstro>
export const WeatherAstro = z.object({
  sunrise: WeatherShortTime,
  sunset: WeatherShortTime,
  moonrise: WeatherShortTime,
  moonset: WeatherShortTime,
  moon_phase: z.string(),
  moon_illumination: z.string().transform(Number),
  is_moon_up: z.number().transform(Boolean),
  is_sun_up: z.number().transform(Boolean),
})

export type WeatherForecastHour = z.infer<typeof WeatherForecastHour>
export const WeatherForecastHour = z.object({
  time_epoch: z.number(),
  time: WeatherShortDateTime,
  temp_c: z.number(),
  temp_f: z.number(),
  is_day: z.number().transform(Boolean),
  condition: WeatherCondition,
  wind_mph: z.number(),
  wind_kph: z.number(),
  wind_degree: z.number(),
  wind_dir: WeatherWindDirection,
  pressure_mb: z.number(),
  pressure_in: z.number(),
  precip_mm: z.number(),
  precip_in: z.number(),
  humidity: z.number().min(0).max(100),
  cloud: z.number().min(0).max(100),
  feelslike_c: z.number(),
  feelslike_f: z.number(),
  windchill_c: z.number(),
  windchill_f: z.number(),
  heatindex_c: z.number(),
  heatindex_f: z.number(),
  dewpoint_c: z.number(),
  dewpoint_f: z.number(),
  will_it_rain: z.number().transform(Boolean),
  chance_of_rain: z.number(),
  will_it_snow: z.number().transform(Boolean),
  chance_of_snow: z.number(),
  vis_km: z.number(),
  vis_miles: z.number(),
  gust_mph: z.number(),
  gust_kph: z.number(),
  uv: z.number(),
})

export type WeatherForecastDay = z.infer<typeof WeatherForecastDay>
export const WeatherForecastDay = z.object({
  date: WeatherShortDate,
  date_epoch: z.number(),
  day: WeatherForecastDayDetails,
  astro: WeatherAstro,
  hour: z.array(WeatherForecastHour),
})

export type WeatherError = z.infer<typeof WeatherError>
export const WeatherError = z.object({
  code: z.number(),
  message: z.string(),
})
