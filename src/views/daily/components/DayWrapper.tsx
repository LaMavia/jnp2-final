import { useAppSelector } from '../../../hooks/redux'
import { selectWeatherDailyForecasts } from '../../../redux/weather/daily/slice'
import { Day } from './Day'
import { VerticalSliceWrapper } from '../../../components/VerticalSlice/components/VerticalSliceWrapper'

export const DayWrapper = () => {
  const forecasts = useAppSelector(selectWeatherDailyForecasts)

  return (
    forecasts.length > 0 && (
      <VerticalSliceWrapper>
        {forecasts.map(forecast => (
          <Day key={forecast.date} forecast={forecast} />
        ))}
      </VerticalSliceWrapper>
    )
  )
}
