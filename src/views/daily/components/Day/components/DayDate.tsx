import { WeatherForecastDay } from '../../../../../api/weather/types/common'
import { getUserLocale } from '../../../../../utils/locale'
import { VerticalSliceTextWrapper } from '../../../../../components/VerticalSlice/components/VerticalSliceTextContainer'
import { VerticalSliceText } from '../../../../../components/VerticalSlice/components/VerticalSliceText'

export const DayDate = ({ forecast }: { forecast: WeatherForecastDay }) => {
  const userLocale = getUserLocale()
  const rawDate = new Date(Date.parse(forecast.date))
  const formattedDate = new Intl.DateTimeFormat(userLocale, {
    day: '2-digit',
    month: '2-digit',
  }).format(rawDate)
  const formattedWeekDay = new Intl.DateTimeFormat(userLocale, {
    weekday: 'long',
  }).format(rawDate)

  return (
    <VerticalSliceTextWrapper gridArea='date'>
      <VerticalSliceText>{formattedDate}</VerticalSliceText>
      <VerticalSliceText>{formattedWeekDay}</VerticalSliceText>
    </VerticalSliceTextWrapper>
  )
}
