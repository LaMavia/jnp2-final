import { Epic, StateObservable, combineEpics, ofType } from 'redux-observable'
import { AppAction, RootState } from '../app/store'
import { Observable, catchError, from, map, mergeMap, of } from 'rxjs'
import { cacheContains, cacheGet } from '../../utils/cache'
import { addBackgroundToCache, fetchBackground, setBackground } from './slice'
import { withLoader } from '../../utils/redux'
import { getPexelsSearch } from '../../api/pexels'

const backgroundFetchEpic: Epic<AppAction> = (
  action$,
  state$: StateObservable<RootState>,
) => {
  const fetchNewBackground$ = (description: string) =>
    withLoader(
      from(getPexelsSearch(description)).pipe(
        map(r => r.photos[0]),
        mergeMap(photo =>
          of(
            setBackground(photo),
            addBackgroundToCache({
              searchPhrase: description,
              photo,
            }),
          ),
        ),
        catchError(exn => {
          console.error(exn)
          return of()
        }),
      ),
    )

  const checkCache$ = (
    description: string,
    { background: { cache } }: RootState,
  ): Observable<AppAction> =>
    cacheContains(cache, description)
      ? of(setBackground(cacheGet(cache, description)!))
      : fetchNewBackground$(description)

  return action$.pipe(
    ofType(fetchBackground.type),
    map(action => action.payload),
    map(description => `forecast weather ${description}`),
    mergeMap(searchPhrase => checkCache$(searchPhrase, state$.value)),
  )
}

export const backgroundEpic = combineEpics(backgroundFetchEpic)
