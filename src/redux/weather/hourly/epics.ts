import { Epic, StateObservable, combineEpics, ofType } from 'redux-observable'
import { AppAction, RootState } from '../../app/store'
import { withLoader } from '../../../utils/redux'
import { catchError, from, map, mergeMap, of } from 'rxjs'
import { cacheContains, cacheGet } from '../../../utils/cache'
import { getWeatherHourly } from '../../../api/weather'
import { WeatherResponseException } from '../../../api/weather/types'
import { pushError } from '../../error/slice'
import { fetchWeatherHourly, hourlyAddToCache, setWeatherHourly } from './slice'

const locationFetchEpic: Epic<AppAction> = (
  action$,
  state$: StateObservable<RootState>,
) => {
  const newSearch$ = (location: string) =>
    withLoader(
      from(getWeatherHourly(location)).pipe(
        map(({ day: forecast, location }) => ({
          forecast,
          location,
        })),
        mergeMap(response =>
          of(
            setWeatherHourly(response),
            hourlyAddToCache({
              location,
              response,
            }),
          ),
        ),
        catchError(exn => {
          console.error(exn)
          const { error }: WeatherResponseException = exn
          return of(pushError(error))
        }),
      ),
    )

  return action$
    .pipe(
      ofType(fetchWeatherHourly.type),
      map(({ payload }) => payload),
      map(newWeatherType => [newWeatherType, state$.value] as const),
    )
    .pipe(
      mergeMap(
        ([
          location,
          {
            weather: {
              hourly: { cache },
            },
          },
        ]) =>
          cacheContains(cache, location)
            ? of(setWeatherHourly(cacheGet(cache, location)!))
            : newSearch$(location),
      ),
    )
}

export const weatherHourlyEpic = combineEpics(locationFetchEpic)
