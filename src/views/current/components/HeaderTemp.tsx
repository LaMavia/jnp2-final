import { styled } from 'styled-components'
import { fontLarge, fontSmall } from '../../../redux/theme/utils/fontSize'
import { useAppSelector } from '../../../hooks/redux'
import { selectWeatherCurrentForecast } from '../../../redux/weather/current/slice'

const StyledContainer = styled.div`
  grid-area: temp;
  color: ${({ theme }) => theme.palette.primary.text};
  text-align: center;
  display: flex;
  flex-flow: column nowrap;
  justify-content: left;
`

const StyledTextSmall = styled.span`
  font-size: ${fontSmall};
`

const StyledTextLarge = styled.span`
  font-size: ${fontLarge};
`

export const HeaderTemp = () => {
  const forecast = useAppSelector(selectWeatherCurrentForecast)

  return (
    forecast && (
      <StyledContainer>
        <StyledTextLarge>{forecast.temperature}</StyledTextLarge>
        <StyledTextSmall>feels like {forecast['feels like']}</StyledTextSmall>
      </StyledContainer>
    )
  )
}
