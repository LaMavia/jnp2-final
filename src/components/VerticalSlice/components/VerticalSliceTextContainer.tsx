import { styled } from 'styled-components'

export const VerticalSliceTextWrapper = styled.div<{ gridArea: string }>`
  grid-area: ${({ gridArea }) => gridArea};
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-evenly;
`
