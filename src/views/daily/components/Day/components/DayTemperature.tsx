import { styled } from 'styled-components'
import { WeatherForecastDay } from '../../../../../api/weather/types/common'
import { ArrowDownIcon } from '../../../../../assets/icons/ArrowDownIcon'
import { fontSmall } from '../../../../../redux/theme/utils/fontSize'
import { ArrowUpIcon } from '../../../../../assets/icons/ArrowUpIcon'

export interface DayTemperatureProps {
  forecast: WeatherForecastDay
}

const OuterContainer = styled.div`
  grid-area: temp;
  width: 100%;
  height: 100%;
  font-size: ${fontSmall};
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;
`
const TemperatureBox = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;

  & svg {
    fill: ${({ theme }) => theme.palette.primary.text};
    height: ${fontSmall};
    width: ${fontSmall};
  }
`
const TemperatureValue = styled.span`
  color: ${({ theme }) => theme.palette.primary.text};
  float: inline-end;
`

export const DayTemperature = ({ forecast }: DayTemperatureProps) => (
  <OuterContainer>
    <TemperatureBox>
      <ArrowUpIcon />
      <TemperatureValue>{forecast.day.maxtemp_c}°C</TemperatureValue>
    </TemperatureBox>
    <TemperatureBox>
      <ArrowDownIcon />
      <TemperatureValue>{forecast.day.mintemp_c}°C</TemperatureValue>
    </TemperatureBox>
  </OuterContainer>
)
