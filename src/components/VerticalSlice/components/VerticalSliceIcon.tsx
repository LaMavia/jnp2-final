import { styled } from 'styled-components'

export const VerticalSliceIcon = styled.img`
  grid-area: icon;
  justify-self: center;
`
