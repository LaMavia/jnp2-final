import { ReactElement } from 'react'
import { useAppSelector } from './hooks/redux'
import { WeatherType, selectWeatherType } from './redux/weather/type/slice'
import { SearchLayout } from './layouts/SearchLayout'
import { CurrentView } from './views/current'
import { DailyView } from './views/daily'
import { HourlyView } from './views/hourly'

export const Router = () => {
  const weatherType = useAppSelector(selectWeatherType)
  const routes: Record<WeatherType, ReactElement> = {
    [WeatherType.Current]: <CurrentView />,
    [WeatherType.Daily]: <DailyView />,
    [WeatherType.Hourly]: <HourlyView />,
  }

  return (
    <SearchLayout>
      {weatherType in routes ? routes[weatherType] : <h1>unknown route</h1>}
    </SearchLayout>
  )
}
