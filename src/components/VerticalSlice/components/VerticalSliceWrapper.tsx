import { styled } from 'styled-components'

export const VerticalSliceWrapper = styled.ul`
  list-style: none;
  display: flex;
  flex-flow: row nowrap;
  width: 100%;
  height: min(600px, 60vh);
  overflow: auto hidden;
  border: 2px solid ${({ theme }) => theme.palette.accent.background};
  margin: 0;
  padding: 0;
`
