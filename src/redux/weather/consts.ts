export const NICE_MAX_AVG_TEMP = 25
export const NICE_MIN_AVG_TEMP = 18
export const NICE_MIN_TEMP = 15
export const NICE_MAX_TEMP = 30
