import { styled } from 'styled-components'
import { useAppSelector } from '../../../hooks/redux'
import { selectWeatherCurrentForecast } from '../../../redux/weather/current/slice'

const StyledSpan = styled.span`
  grid-area: date-time;
  color: ${({ theme }) => theme.palette.secondary.text};
  padding: 5px;
  text-align: center;
`

export const HeaderDate = () => {
  const forecast = useAppSelector(selectWeatherCurrentForecast)

  return forecast && <StyledSpan>{forecast.date}</StyledSpan>
}
