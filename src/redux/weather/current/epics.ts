import { Epic, StateObservable, combineEpics, ofType } from 'redux-observable'
import { AppAction, RootState } from '../../app/store'
import { catchError, from, map, mergeMap, of, switchMap } from 'rxjs'
import { addToCache, fetchWeatherCurrent, setWeatherCurrent } from './slice'
import { getWeatherCurrent } from '../../../api/weather'
import { WeatherResponseException } from '../../../api/weather/types'
import { pushError } from '../../error/slice'
import { cacheContains, cacheGet } from '../../../utils/cache'
import { withLoader } from '../../../utils/redux'
import { fetchBackground } from '../../background/slice'

const locationFetchEpic: Epic<AppAction> = (
  action$,
  state$: StateObservable<RootState>,
) => {
  const newSearch$ = (location: string) =>
    withLoader(
      from(getWeatherCurrent(location)).pipe(
        mergeMap(response =>
          of(
            setWeatherCurrent(response.current),
            addToCache({
              location,
              response,
            }),
            fetchBackground(response.current.condition.text),
          ),
        ),
        catchError(exn => {
          console.error(exn)
          const { error }: WeatherResponseException = exn
          return of(pushError(error))
        }),
      ),
    )

  return action$.pipe(
    ofType(fetchWeatherCurrent.type),
    map(({ payload }) => payload),
    map(
      forceFetch =>
        ({
          forceFetch,
          cache: state$.value.weather.current.cache,
          location: state$.value.location.location,
        } as const),
    ),
    switchMap(({ forceFetch, cache, location }) =>
      !forceFetch && cacheContains(cache, location)
        ? of(setWeatherCurrent(cacheGet(cache, location)!.current))
        : newSearch$(location),
    ),
  )
}

export const weatherCurrentEpic = combineEpics(locationFetchEpic)
