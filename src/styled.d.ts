import 'styled-components'
import { Theme } from './redux/theme/types'

declare module 'styled-components' {
  export type DefaultTheme = Theme
}
