import { Epic, StateObservable, combineEpics, ofType } from 'redux-observable'
import { AppAction, RootState } from '../../app/store'
import { filter, map } from 'rxjs'
import { setWeatherType } from './slice'
import { setLocation } from '../../location/slice'

const refetchWeatherOnNavigationEpic: Epic<AppAction> = (
  action$,
  state$: StateObservable<RootState>,
) =>
  action$.pipe(
    ofType(setWeatherType.type),
    map(() => state$.value.location.location),
    filter(location => location !== ''),
    map(setLocation),
  )

export const weatherTypeEpic = combineEpics(refetchWeatherOnNavigationEpic)
