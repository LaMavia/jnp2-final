import { z } from 'zod'
import { WeatherWindDirection } from '../../../api/weather/types/common'
import { getUserLocale } from '../../../utils/locale'

export type WeatherCurrentForecast = z.infer<typeof WeatherCurrentForecast>
export const WeatherCurrentForecast = z
  .object({
    dateTime: z
      .string()
      .datetime()
      .transform(dateTime => new Date(dateTime)),
    temp: z.number(),
    tempFeelsLike: z.number(),
    tempUnit: z.enum(['C', 'F']),
    description: z.string(),
    pressure: z.number(),
    pressureUnit: z.enum(['mbar', 'in']),
    precipitation: z.number(),
    precipitationUnit: z.enum(['mm', 'in']),
    humidity: z.number().min(0).max(100),
    windDirection: WeatherWindDirection,
    windSpeed: z.number().min(0),
    windSpeedUnit: z.enum(['km/h', 'mph']),
    uv: z.number().min(0).max(11),
    cloudCoverage: z.number().min(0).max(100),
  })
  .transform(
    ({
      dateTime,
      temp,
      tempUnit,
      tempFeelsLike,
      pressure,
      pressureUnit,
      precipitation,
      precipitationUnit,
      windDirection,
      windSpeed,
      windSpeedUnit,
      cloudCoverage,
      humidity,
      ...forecast
    }) => ({
      ...forecast,
      date: new Intl.DateTimeFormat(getUserLocale(), {
        dateStyle: 'full',
        timeStyle: 'medium',
      }).format(dateTime),
      temperature: `${temp}°${tempUnit}`,
      'feels like': `${tempFeelsLike}°${tempUnit}`,
      pressure: `${pressure}${pressureUnit}`,
      precipitation: `${precipitation}${precipitationUnit}`,
      wind: `${windDirection} ${windSpeed}${windSpeedUnit}`,
      'cloud coverage': `${cloudCoverage}%`,
      humidity: `${humidity}%`,
    }),
  )
