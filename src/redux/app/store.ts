import { configureStore } from '@reduxjs/toolkit'
import { Epic, createEpicMiddleware } from 'redux-observable'
import { rootEpic } from './epics'
import { ThemeAction, themeReducer, themeStoreName } from '../theme/slice'
import {
  LocationAction,
  locationReducer,
  locationStoreName,
} from '../location/slice'
import { ErrorAction, errorReducer, errorStoreName } from '../error/slice'
import {
  WeatherAction,
  weatherReducer,
  weatherStoreName,
} from '../weather/slice'
import {
  LoadingAction,
  loadingReducer,
  loadingStoreName,
} from '../loading/slice'
import {
  BackgroundAction,
  backgroundReducer,
  backgroundStoreName,
} from '../background/slice'

export const makeStore = () => {
  const epicMiddleware = createEpicMiddleware()
  const store = configureStore({
    reducer: {
      [themeStoreName]: themeReducer,
      [locationStoreName]: locationReducer,
      [errorStoreName]: errorReducer,
      [weatherStoreName]: weatherReducer,
      [loadingStoreName]: loadingReducer,
      [backgroundStoreName]: backgroundReducer,
    },
    middleware: [epicMiddleware],
  })

  epicMiddleware.run(rootEpic as Epic)

  return store
}

type StoreType = ReturnType<typeof makeStore>
export type RootState = ReturnType<StoreType['getState']>
export type AppDispatch = StoreType['dispatch']
export type AppAction =
  | LocationAction
  | ThemeAction
  | ErrorAction
  | WeatherAction
  | LoadingAction
  | BackgroundAction
